package main;
import java.util.Calendar;

import model.TheaterManagement;


public class mainTheater {
	public static void main(String[] args){
		Calendar c = Calendar.getInstance();
		//Sunday 1 ,  Monday 2 ,Tuesday 3 ,Wednesday 4 ,Thursday 5,Friday 6,Saturday 7  
		TheaterManagement theater = new TheaterManagement(2);
		System.out.println("Test nomal day");
		System.out.println(theater.getDataSeatPrice(2, 1));
		System.out.println("buy ticket O11,O12 count 1	: "+theater.buySeat(1,'O', 11,12));
		System.out.println("buy ticket price 20.0,50.0	: "+theater.buySeat(20.0,50.0));
		System.out.println("buy ticket O12,O13 		: "+theater.buySeat('O', 12,13));
		System.out.println("buy ticket E1	: "+theater.buySeat('E', 1));
		System.out.println("buy ticket O11,O12 count 1	: "+theater.buySeat(1,'O', 11,12));
		System.out.println("buy ticket O12 		: "+theater.buySeat('O', 12)+"  << don't have count for ticket"); //search count = 3;
		System.out.println("buy ticket D1,D2 count 3: "+theater.buySeat(3,'D', 1,2)+" << don't have count 3 in normal day"); //search count = 3;
		System.out.println(theater.getDataSeatPrice(2, 1));
		System.out.println("Test holiday day");
		System.out.println("set new day : Sunday");
		theater.setDay(7);
		System.out.println("buy ticket A3 A4 count 3	: "+theater.buySeat(3,'A', 3,4));
		System.out.println("buy ticket A5 A6 count 3	: "+theater.buySeat(3,'A', 5,6));
		System.out.println("buy ticket A7 A8 count 3	: "+theater.buySeat(3,'A', 7,8));

		System.out.println(theater.getDataSeatPrice(7, 3));

		// -64
	}
}
