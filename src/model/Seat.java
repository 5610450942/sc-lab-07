package model;

public class Seat {
	private char row ;
	private int collum;
	private int count;
	private double price;
	private int day;
	public Seat(int day ,int count ,char row,int collum,double price){
		this.day = day;
		this.count = count;
		this.row = row;
		this.collum = collum;
		this.price = price;
	}
 public boolean equals(Object other){
	 if(other == null){return false;}
	 if(!(other instanceof Seat)){return false;};
	 Seat o = (Seat)other;
	 return row == o.row && collum == o.collum && count == o.count&& price == o.price;
 }
public char getRow() {
	// TODO Auto-generated method stub
	return row;
}
public int getCount() {
	// TODO Auto-generated method stub
	return count;
}
public int getCollum() {
	// TODO Auto-generated method stub
	return collum;
}
public int getDay() {
	// TODO Auto-generated method stub
	return day;
}
}
