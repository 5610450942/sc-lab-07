package model;
import java.util.ArrayList;


public class TheaterManagement {
	ArrayList<Seat> seats;
	DataSeatPrice dataSeats; 
	private int day = 0;
	public TheaterManagement(int day){ 
		dataSeats = new DataSeatPrice();
		seats = new ArrayList<Seat>();
		this.day =  day;
	}

	public double buySeat(int count, char row, int collum) { // input count row collum
		if(checkLimeitCount(count)){
			Seat seat =new Seat(this.day,count,row,collum-1,0);
			if(seats.contains(seat) == false){
				seats.add(seat);			
				return dataSeats.ticketPrices[row-65][collum];
			}
		}
		return 0;	
	}
	public double buySeat(int count, char row, int collum1,int collum2) {  // input count row collum 1 collum 2
		return buySeat(count,row,collum1)+buySeat(count,row,collum2);		
	}
	
	public String buySeat( char row, int collum) {
		int count = 1;
		while(checkSeat(count,row,collum-1) == false){ // search count for empty seat
			count++;
		}
		if(checkLimeitCount(count)){
			Seat seat =new Seat(this.day,count,row,collum-1,0);
			if(seats.contains(seat)== false){
				seats.add(seat);			
				return dataSeats.ticketPrices[row-65][collum]+" count "+count;
			}
		}
		return "0.0";
		
	}
	public String buySeat(char row, int collum1,int collum2) {
		int count = 1;
		while(checkSeat(count,row,collum1-1) == false || checkSeat(count,row,collum2-1) == false){ // search count for empty seat1 and seat2
			count++;
		}

		if(checkLimeitCount(count)){
			Seat seat1 =new Seat(this.day,count,row,collum1-1,0);
			Seat seat2 =new Seat(this.day,count,row,collum2-1,0);
			if(seats.contains(seat1) == false && seats.contains(seat2) == false){
				seats.add(seat1);
				seats.add(seat2);			
				return dataSeats.ticketPrices[row-65][collum1] + dataSeats.ticketPrices[row-65][collum1]+" count "+count;
			}
	}
		return "0.0";
	}
		public String buySeat(double price) {
		int count = 1;
		for(int i = 0 ; i < dataSeats.ticketPrices.length;i++){
			for(int j = 0 ; j < dataSeats.ticketPrices[i].length;j++){
				if(dataSeats.ticketPrices[i][j]== price ){
					if(checkLimeitCount(count)){
						Seat seat1 =new Seat(this.day,count,(char)(i+65),j-1,0);
						if(seats.contains(seat1) == false ){
							seats.add(seat1);
							return dataSeats.ticketPrices[i][j] +" "+(char)(i+65)+""+j+" count "+count;
						}
					}
				}
			}
		}
		return "0.0";
	}
	public String buySeat(double price1,double price2) {
		return  buySeat(price1)+" "+buySeat(price2);
	}
	public String getDataSeatPrice(int day,int count){
		
		//show string ticketPrice
		String[] rowString = {"A ","B ","C ","D ","E ","F ","G ","H ","i ","J ","K ","L ","M ","N ","O "};
		String str = "day "+this.day+"count "+count+"\n"+"row:col"+"	1	2	3	4	5	6	7	8	9	10	11	12	13	14	15	16	17	18	19	20\n";
		for(int i = 0 ;i < dataSeats.ticketPrices.length ;i++ ){
			str += rowString[i];
			for(int j = 0 ;j < dataSeats.ticketPrices[i].length ;j++ ){
				if(checkSeat(count, (char)(i+65), j) ){
					if(dataSeats.ticketPrices[i][j] != 0.0){
					str += "	"+dataSeats.ticketPrices[i][j];}
					else{str += "	X";}
				}
				else{
					str += "      0.0";
				}
			}
			str +="\n";
		}
		return str+"------------------------------------------------------------------------------------------------------------------------------------------------------------------------";
	}
	private boolean checkSeat(int count, char row, int collum){ 
		//System.out.print("check"+count);
		// return false if seat have count,row,collum equal parameter
		for(Seat s:seats){
			if(s.getCount() == count && s.getRow()== row && s.getCollum()== collum && this.day == s.getDay()){
				return false;
			}
		}
		return true;
	}
	private boolean checkLimeitCount(int count){
		//return true if day 1,7 is Holiday have 3 count
		if(count <= 0 || day <= 0)  throw new IllegalArgumentException("day and count must be positive !! ");
		if((day == 1 || day==7 )&&(count <= 3)){return true;}
		//return true if day 2-5 is normal have 2 count
		if((day != 1 || day!=7 )&&(count <= 2)){return true;}
		return false;
	}
	public void setDay(int day){this.day = day;}
}
